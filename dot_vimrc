" Install vim-plug if not found
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif

" Run PlugInstall if there are missing plugins
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \| PlugInstall --sync | source $MYVIMRC
  \| endif

function! PlugHookPreBase()
  " line numbers
  set number

  " faster navigation
  " normal mode:
  nnoremap <c-j> 5j
  nnoremap <c-k> 5k
  nnoremap <c-h> 10j
  nnoremap <c-l> 10k
  " visual mode:
  xnoremap <c-j> 5j
  xnoremap <c-k> 5k
  xnoremap <c-h> 10j
  xnoremap <c-l> 10k

  " turn off ex mode
  nnoremap Q <nop>

  " turn off replace mode
  nnoremap r <nop>
  nnoremap R <nop>

  " use <esc>
  inoremap <c-c> <Esc>

  " use rg for grep
  set grepprg=rg\ --vimgrep\ --no-heading\ --smart-case
  set grepformat=%f:%l:%c:%m,%f:%l:%m
  map <Leader>S :silent grep<Space>
endfunction

function! PlugHookPreArecarnCloseBuffersVim()
  map <Leader>ba :Bdelete all<CR>
  map <Leader>bh :Bdelete hidden<CR>
  " <Leader>bl is used in tpope/vim-fugitive
  map <Leader>bm :Bdelete menu<CR>
  map <Leader>bn :Bdelete nameless<CR>
  map <Leader>bo :Bdelete other<CR>
  map <Leader>bs :Bdelete select<CR>
  map <Leader>bt :Bdelete this<CR>
endfunction

function! PlugHookPreBenknobleVimAutoOrigami()
  augroup autofoldcolumn
    au!
    " Or whatever autocmd-events you want
    au CursorHold,BufWinEnter,WinEnter * AutoOrigamiFoldColumn
  augroup END
endfunction

function! PlugHookPreBroothFarVim()
  map <Leader>/ :Farf<CR>
  map <Leader>? :Farr<CR>

  set lazyredraw            " improve scrolling performance when navigating through large results
  set ignorecase smartcase  " ignore case only when the pattern contains no capital letters

  let g:far#enable_undo = 1
  let g:far#glob_mode = 'native'
  let g:far#open_in_parent_window = 1
  let g:far#preview_window_height = 20
  let g:far#source = 'rgnvim'
  let g:far#mapping = {
      \ "preview_scroll_up" : "",
      \ "preview_scroll_down" : "",
      \ }
endfunction

function! PlugHookPreDenseAnalysisAle()
  let g:ale_fix_on_save = 1
  " let g:ale_linters = {
  "       \   'go': ['gopls'],
  "       \   'rust': ['rls'],
  "       \   'typescript': ['tslint'],
  "       \   'vue': ['vls'],
  "       \}
  let g:ale_fixers = {
        \   '*': ['remove_trailing_lines', 'trim_whitespace'],
        \   'css': ['eslint'],
        \   'go': ['gofmt'],
        \   'html': ['eslint'],
        \   'javascript': ['prettier'],
        \   'json': ['eslint'],
        \   'ruby': ['rubocop'],
        \   'rust': ['rustfmt'],
        \   'typescript': ['prettier'],
        \   'vue': ['prettier'],
        \}
  let g:ale_disable_lsp = 1
  let g:ale_use_neovim_diagnostics_api = 1

  " vim-airline integration
  let g:airline#extensions#ale#enabled = 1

  " vim only

  " map <Leader>rd :ALEGoToDefinition<CR>
  " map <Leader>rr :ALEFindReferences<CR>
  " map <Leader>rt :ALEDetail<CR>
  " map <Leader>re :ALEHover<CR>
  " map <Leader>rs :ALESymbolSearch<Space>
  " map <Leader>ra :ALECodeAction<CR>
  " map <Leader>rn :ALENext<CR>
  " map <Leader>rp :ALEPrevious<CR>
  " map <Leader>ri :ALEInfoToClipboard<CR>

  " configure hover
  " let g:ale_set_balloons = 1
  " let g:ale_hover_to_preview = 1

  " completion
  " let g:ale_completion_enabled = 1
  " set omnifunc=ale#completion#OmniFunc

  " auto-import typescript modules
  " let g:ale_completion_autoimport = 1

  " turn off aggressive autocompletion
  " set completeopt=menu,menuone,preview,noselect,noinsert
endfunction

function! PlugHookPreGruvboxCommunityGruvbox()
  let g:gruvbox_italic=1
  let g:gruvbox_contrast_dark='soft'
  let g:gruvbox_contrast_light='soft'

  " set true colors in vim inside tmux
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
  set termguicolors

  set background=light

  " handling setting and unsetting BAT_THEME for fzf.vim
  augroup update_bat_theme
    autocmd!
    autocmd colorscheme * call ToggleBatEnvVar()
  augroup end
  function ToggleBatEnvVar()
    if (&background == "light")
        let $BAT_THEME='gruvbox-light'
    else
        let $BAT_THEME='gruvbox-dark'
    endif
  endfunction
endfunction

function! PlugHookPreHaya14busaIncSearchVim()
  map /  <Plug>(incsearch-forward)
  map ?  <Plug>(incsearch-backward)
  map g/ <Plug>(incsearch-stay)

  set hlsearch
  let g:incsearch#auto_nohlsearch = 1
  map n  <Plug>(incsearch-nohl-n)
  map N  <Plug>(incsearch-nohl-N)
  map *  <Plug>(incsearch-nohl-*)
  map #  <Plug>(incsearch-nohl-#)
  map g* <Plug>(incsearch-nohl-g*)
  map g# <Plug>(incsearch-nohl-g#)
endfunction

function! PlugHookPreJunegunnFzfVim()
  map <Space> <Leader>

  map <Leader>eq :q<CR>
  map <Leader>eQ :q!<CR>
  map <Leader>ew :w<CR>
  map <Leader>eW :w!<CR>
  " <Leader>ee is used in lambdalisue/fern.vim
  " <Leader>r is used in dense-analysis/ale
  map <Leader>t :BTags<Space>
  map <Leader>T :Tags<Space>

  map <Leader>o :BCommits<CR>
  map <Leader>O :Commits<CR>

  " <Leader>a is used in valloric/listtoggle
  map <Leader>s :Rg<Space>
  " <Leader>d is used in valloric/listtoggle
  map <Leader>f :GFiles<CR>
  map <Leader>F :Files<CR>
  " <Leader>g is used in tpope/vim-fugitive
  " <Leader>G is used in rbong/vim-flog

  map <Leader>hh :Helptags<CR>
  map <Leader>hi :History<CR>
  map <Leader>h: :History:<CR>
  map <Leader>h/ :History/<CR>
  map <Leader>l :BLines<CR>
  map <Leader>L :Lines<CR>
  map <Leader>: :Commands<CR>

  " <Leader>z is used in junegunn/goyo.vim
  " <Leader>c is used in dense-analysis/ale
  " <Leader>v is used in vimwiki/vimwiki
  " <Leader>b is used in asheq/close-buffers.vim
  map <Leader>B :Buffers<CR>

  " <Leader>n is used in ledger/vim-ledger
  map <Leader>m :Maps<CR>
  map <Leader>M :Marks<CR>
  " <Leader>/ is used in brooth/far.vim
  " <Leader>? is used in brooth/far.vim

  let g:fzf_layout = { 'window': { 'width': 1, 'height': 1 } }
  let g:fzf_preview_window = ['down,60%']

  " [Buffers] Jump to the existing window if possible
  let g:fzf_buffers_jump = 1

  " [[B]Commits] Customize the options used by 'git log':
  let g:fzf_commits_log_options = '--graph --color=always --format="%C(auto)%h%d %s %C(black)%C(bold)%cr"'

  " [Tags] Command to generate tags file
  let g:fzf_tags_command = 'ctags -R'

  " [Commands] --expect expression for directly executing the command
  let g:fzf_commands_expect = 'ctrl-x'

  " [Rg] pass arguments to rg (such as --hidden)
  " To customize: lift from source, then remove double-dash and shellescape
  command! -bang -nargs=* Rg call fzf#vim#grep("rg --column --line-number --no-heading --color=always --smart-case ".<q-args>, 1, fzf#vim#with_preview(), <bang>0)
endfunction

function! PlugHookPreJunegunnGoyoVim()
  map <Leader>z :Goyo<CR>

  let g:goyo_linenr = 1
  let g:goyo_height = 100
  " let g:goyo_width = 100

  " Run logic when goyo is enabled / disabled
  function! s:goyo_enter()
    if executable('tmux') && strlen($TMUX)
      silent !tmux set status off
      silent !tmux list-panes -F '\#F' | grep -q Z || tmux resize-pane -Z
    endif
    set noshowmode
    set noshowcmd
    set scrolloff=999
    Limelight
    SignifyEnable
  endfunction

  function! s:goyo_leave()
    if executable('tmux') && strlen($TMUX)
      silent !tmux set status on
      silent !tmux list-panes -F '\#F' | grep -q Z && tmux resize-pane -Z
    endif
    set showmode
    set showcmd
    set scrolloff=1 " set by tpope/vim-sensible
    Limelight!
    SignifyEnable
  endfunction

  autocmd! User GoyoEnter nested call <SID>goyo_enter()
  autocmd! User GoyoLeave nested call <SID>goyo_leave()
endfunction

function! PlugHookPreJunegunnLimelight()
endfunction

function! PlugHookPreJunegunnRainbowParenthesesVim()
  map <Leader>p :RainbowParentheses!!<CR>
endfunction

function! PlugHookPreJustinmkVimSneak()
  map f <Plug>Sneak_s
  map F <Plug>Sneak_S

  let g:sneak#s_next = 1
endfunction

function! PlugHookPreLambdalisueFernVim()
  map <Leader>ee :Fern . -drawer -reveal=% -width=40 -toggle<CR>

  let g:fern#renderer = "nerdfont"

  function! s:init_fern() abort
    nmap <buffer><nowait> <C-j> 5j
    nmap <buffer><nowait> <C-k> 5k
    nmap <buffer><nowait> zt zt
    nmap <buffer><nowait> zz zz
    nmap <buffer><nowait> zb zb
  endfunction

  augroup fern-custom
    autocmd! *
    autocmd FileType fern call s:init_fern()
  augroup END
endfunction

function! PlugHookPreLedgerVimLedger()
  map <Leader>na :LedgerAlignBuffer<CR>
  map <Leader>nt :put=strftime('%Y/%m/%d')<CR>

  autocmd BufWritePost *.journal LedgerAlignBuffer

  " use "{" and "}" to move by paragraphs
  au FileType ledger noremap { ?^\d<CR>
  au FileType ledger noremap } /^\d<CR>
endfunction

function! PlugHookPrePseewaldVimAnyfold()
  filetype plugin indent on " required
  syntax on                 " required

  autocmd Filetype * if &ft == "vimwiki" | call SetupFoldExclude() | elseif &ft == "git" | call SetupFoldGit() | else | call SetupFoldAny() | endif
  function! SetupFoldExclude()
    set nofoldenable " disable fold
    " vimwiki will handle its own folding
  endfunction
  function! SetupFoldGit()
    set foldenable " enable fold
    set foldlevel=0
  endfunction
  function! SetupFoldAny()
    set nofoldenable " disable fold
    AnyFoldActivate
    set foldlevel=99
  endfunction
endfunction

function! PlugHookPrePsliwkaVimSmoothie()
  " speed at start of animation; default value: 10
  let g:smoothie_speed_linear_factor = 40
  " speed at end of animation; default value: 10
  let g:smoothie_speed_constant_factor = 40
endfunction

function! PlugHookPreRbongVimFlog()
  map <Leader>G :Flogsplit<CR>
endfunction

function! PlugHookPreRuanylVimSortImports()
  " let g:import_sort_auto = 1
endfunction

" function! PlugHookRrethyVimHexokinase()
"   let g:Hexokinase_highlighters = ['sign_column']
" endfunction

function! PlugHookPreSheerunVimPolyglot()
  " csv
  " let g:csv_nomap_space = 1
  " let g:csv_delim=','
  let g:polyglot_disabled = ['csv']

  " vim
  " do not conceal
  let g:vim_markdown_conceal = 0
  let g:vim_markdown_conceal_code_blocks = 0
endfunction

function! PlugHookPreSvermeulenVimCutlass()
  " x to delete directly without cutting
  " use dl to delete a character
  nnoremap x d
  xnoremap x d
  nnoremap xx dd
  nnoremap X D
endfunction

function! PlugHookPreSvermeulenVimSubversive()
  " s to substitute
  " use cl to change a letter
  nmap s <plug>(SubversiveSubstitute)
  nmap ss <plug>(SubversiveSubstituteLine)
  nmap S <plug>(SubversiveSubstituteToEndOfLine)
endfunction

function! PlugHookPreSvermeulenVimYoink()
  map <Leader>y :Yanks<CR>
  map <Leader>Y :ClearYanks<CR>

  let g:yoinkIncludeDeleteOperations = 1

  nmap p <plug>(YoinkPaste_p)
  nmap P <plug>(YoinkPaste_P)

  " Also replace the default gp with yoink paste so we can toggle paste in this case too
  nmap gp <plug>(YoinkPaste_gp)
  nmap gP <plug>(YoinkPaste_gP)

  nmap [p <plug>(YoinkRotateBack)
  nmap ]p <plug>(YoinkRotateForward)
endfunction

function! PlugHookPreTpopeVimFugitive()
  map <Leader>gg :G<CR>

  map <Leader>gw :GBrowse<CR>
  map <Leader>ge :Gedit<Space>
  map <Leader>gt :Git difftool<CR>
  map <Leader>gs :Gvdiffsplit<CR>
  map <Leader>gd :Git diff<CR>
  map <Leader>gh :Ghdiffsplit<CR>
  map <Leader>gl :Git log<CR>
  map <Leader>gb :Git blame<CR>
  map <Leader>gm :Git mergetool<CR>

  map <Leader>gM :GMove<Space>
  map <Leader>gR :GRename<Space>
  map <Leader>gD :GDelete<CR>
  map <Leader>gV :GRemove<CR>

  " set split to vertical
  set diffopt+=vertical

  " delete buffers
  autocmd BufReadPost fugitive://* set bufhidden=delete

  " to compare branches: :G difftool --name-only _from_ _to_
  " use <Leader>d to compare diffs in difftool
  " keep until part of plugin: https://github.com/tpope/vim-fugitive/issues/132#issuecomment-570844756
  fun! DiffCurrentQuickfixEntry() abort
    cc
    let qf = getqflist({'context': 0, 'idx': 0})
    if get(qf, 'idx') && type(get(qf, 'context')) == type({}) && type(get(qf.context, 'items')) == type([])
      let diff = get(qf.context.items[qf.idx - 1], 'diff', [])
      for i in reverse(range(len(diff)))
        exe (i ? 'rightbelow' : 'leftabove') 'vert diffsplit' fnameescape(diff[i].filename)
        wincmd p
      endfor
    endif
  endfun
  " map <Leader>d :call DiffCurrentQuickfixEntry()<CR>
endfunction

function! PlugHookPreTpopeVimUnimpaired()
  nnoremap td :tabclose<CR>
  nnoremap tn :tabnew<CR>
  nnoremap yc :set colorcolumn=65<CR>
endfunction

function! PlugHookPreValloricListToggle()
  let g:lt_quickfix_list_toggle_map = '<Leader>at'
  let g:lt_location_list_toggle_map = '<Leader>dt'

  " quickfix
  map <Leader>ao :copen<CR>
  map <Leader>ac :cclose<CR>
  map <Leader>an :cnext<CR>
  map <Leader>ap :cprev<CR>
  map <Leader>ab :cb<CR>

  " loclist
  map <Leader>do :lopen<CR>
  map <Leader>dc :lclose<CR>
  map <Leader>dn :lnext<CR>
  map <Leader>dp :lprev<CR>
  map <Leader>db :lb<CR>
endfunction

function! PlugHookPreVimAirlineVimAirline()
  let g:airline_powerline_fonts = 1
  let g:airline#extensions#tabline#enabled = 1
endfunction

function! PlugHookPreVimwikiVimwiki()
  map <Leader>vx :VimwikiToggleListItem<CR>
  map <Leader>vh :VimwikiAll2HTML!<CR>
  map <Leader>vs :VimwikiSearchTags<Space>
  map <Leader>vt :VimwikiTable<CR>

  au FileType vimwiki setlocal shiftwidth=2 tabstop=2 expandtab

  let g:vimwiki_folding = 'syntax'

  " wiki
  let g:vimwiki_list = [
  \ {
  \    'automatic_nested_syntaxes': 1,
  \    'auto_export': 1,
  \    'auto_tags': 1,
  \    'auto_toc': 1,
  \    'path': '$HOME/.local/share/notes/content',
  \    'path_html': '$HOME/.local/share/notes/public',
  \    'template_default': 'default',
  \    'template_ext': '.html',
  \    'template_path': '$HOME/.local/share/notes/templates',
  \ },
  \ {
  \    'automatic_nested_syntaxes': 1,
  \    'auto_export': 1,
  \    'auto_tags': 1,
  \    'auto_toc': 1,
  \    'path': '$HOME/.local/share/work/content',
  \    'path_html': '$HOME/.local/share/work/public',
  \    'template_default': 'default',
  \    'template_ext': '.html',
  \    'template_path': '$HOME/.local/share/work/templates',
  \ },
  \ ]
endfunction

function! PlugHookPostGruvboxCommunityGruvbox()
  colorscheme gruvbox
endfunction

function! PlugHookPostNeovimLspconfig()
  " nnoremap <Leader>rd <cmd>lua vim.lsp.buf.definition()<CR>
  " nnoremap <Leader>rc <cmd>lua vim.lsp.buf.declaration()<CR>
  " nnoremap <Leader>rr <cmd>lua vim.lsp.buf.references()<CR>
  " nnoremap <Leader>ri <cmd>lua vim.lsp.buf.implementation()<CR>
  " nnoremap <Leader>re <cmd>lua vim.lsp.buf.hover()<CR>
  " nnoremap <Leader>rh <cmd>lua vim.lsp.buf.signature_help()<CR>
  " nnoremap <Leader>ra <cmd>lua vim.lsp.buf.code_action()<CR>
  " nnoremap <Leader>rm <cmd>lua vim.lsp.buf.rename()<CR>
  " nnoremap <Leader>rf <cmd>lua vim.lsp.buf.formatting()<CR>
  " nnoremap <Leader>rp <cmd>lua vim.lsp.diagnostic.goto_prev()<CR>
  " nnoremap <Leader>rn <cmd>lua vim.lsp.diagnostic.goto_next()<CR>

lua << EOF
local nvim_lsp = require('lspconfig')

nvim_lsp.solargraph.setup{}
nvim_lsp.tsserver.setup{}
nvim_lsp.vuels.setup{}

-- -- Use an on_attach function to only map the following keys
-- -- after the language server attaches to the current buffer
-- local on_attach = function(client, bufnr)
--   local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
--   local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end
--
--   -- Enable completion triggered by <c-x><c-o>
--   buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')
--
--   -- Mappings.
--   local opts = { noremap=true, silent=true }
--
--   -- See `:help vim.lsp.*` for documentation on any of the below functions
--   buf_set_keymap('n', '<space>rd', '<cmd>lua vim.lsp.buf.definition()<CR>', opts)
--   buf_set_keymap('n', '<space>rD', '<cmd>lua vim.lsp.buf.declaration()<CR>', opts)
--   buf_set_keymap('n', '<space>re', '<cmd>lua vim.lsp.buf.hover()<CR>', opts)
--   buf_set_keymap('n', '<space>ri', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
--   buf_set_keymap('n', '<space>rh', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
--   -- buf_set_keymap('n', '<space>rwa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
--   -- buf_set_keymap('n', '<space>rwr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
--   -- buf_set_keymap('n', '<space>rwl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
--   buf_set_keymap('n', '<space>rt', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
--   buf_set_keymap('n', '<space>ra', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
--   buf_set_keymap('n', '<space>rc', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
--   buf_set_keymap('n', '<space>rr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
--   buf_set_keymap('n', '<space>rl', '<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>', opts)
--   buf_set_keymap('n', '<space>rp', '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>', opts)
--   buf_set_keymap('n', '<space>rn', '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>', opts)
--   buf_set_keymap('n', '<space>ro', '<cmd>lua vim.lsp.diagnostic.set_loclist()<CR>', opts)
--   buf_set_keymap('n', '<space>rf', '<cmd>lua vim.lsp.buf.formatting()<CR>', opts)
-- end
--
-- -- Use a loop to conveniently call 'setup' on multiple servers and
-- -- map buffer local keybindings when the language server attaches
-- --
-- -- To debug, check: ~/.cache/nvim/lsp.log
-- -- local servers = { 'rls', 'solargraph', 'tsserver', 'vuels' }
-- local servers = { 'rls', 'solargraph', 'tsserver' }
-- for _, lsp in ipairs(servers) do
--   nvim_lsp[lsp].setup {
--     on_attach = on_attach,
--     flags = {
--       debounce_text_changes = 150,
--     }
--   }
-- end
EOF
endfunction

call PlugHookPreBase()
call PlugHookPreArecarnCloseBuffersVim()
call PlugHookPreBenknobleVimAutoOrigami()
call PlugHookPreBroothFarVim()
call PlugHookPreDenseAnalysisAle()
call PlugHookPreGruvboxCommunityGruvbox()
call PlugHookPreHaya14busaIncSearchVim()
call PlugHookPreJunegunnFzfVim()
call PlugHookPreJunegunnGoyoVim()
call PlugHookPreJunegunnLimelight()
call PlugHookPreJunegunnRainbowParenthesesVim()
call PlugHookPreLambdalisueFernVim()
call PlugHookPreLedgerVimLedger()
call PlugHookPrePseewaldVimAnyfold()
call PlugHookPrePsliwkaVimSmoothie()
call PlugHookPreRbongVimFlog()
call PlugHookPreRuanylVimSortImports()
" call PlugHookRrethyVimHexokinase()
" call PlugHookPreSheerunVimPolyglot()
call PlugHookPreSvermeulenVimCutlass()
call PlugHookPreSvermeulenVimSubversive()
call PlugHookPreSvermeulenVimYoink()
call PlugHookPreTpopeVimFugitive()
call PlugHookPreTpopeVimUnimpaired()
call PlugHookPreValloricListToggle()
call PlugHookPreVimAirlineVimAirline()
call PlugHookPreVimwikiVimwiki()

call plug#begin('~/.vim/plugged')

Plug 'arecarn/vim-fold-cycle'
Plug 'asheq/close-buffers.vim'
Plug 'benknoble/vim-auto-origami'
Plug 'blueyed/vim-diminactive'
Plug 'bronson/vim-visual-star-search'
Plug 'brooth/far.vim'
Plug 'christoomey/vim-system-copy'
Plug 'dense-analysis/ale'
Plug 'davinche/godown-vim'
Plug 'editorconfig/editorconfig-vim'
Plug 'gruvbox-community/gruvbox'
Plug 'haya14busa/incsearch.vim'
Plug 'idanarye/vim-merginal'
Plug 'juanchanco/vim-jbuilder'
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
Plug 'junegunn/rainbow_parentheses.vim'
Plug 'kshenoy/vim-signature'
Plug 'lambdalisue/nerdfont.vim'
Plug 'lambdalisue/fern.vim'
Plug 'lambdalisue/fern-bookmark.vim'
Plug 'lambdalisue/fern-git-status.vim'
Plug 'lambdalisue/fern-hijack.vim'
Plug 'lambdalisue/fern-mapping-mark-children.vim'
Plug 'lambdalisue/fern-mapping-quickfix.vim'
Plug 'lambdalisue/fern-renderer-nerdfont.vim'
Plug 'ledger/vim-ledger'
Plug 'liuchengxu/vista.vim'
Plug 'lumakernel/fern-mapping-fzf.vim'
Plug 'mattn/emmet-vim'
Plug 'matze/vim-move'
Plug 'mg979/vim-visual-multi'
Plug 'mhinz/vim-signify'
Plug 'michaeljsmith/vim-indent-object'
Plug 'neovim/nvim-lspconfig'
Plug 'nicwest/vim-http'
Plug 'pseewald/vim-anyfold'
Plug 'psliwka/vim-smoothie'
Plug 'rbong/vim-flog'
Plug 'roxma/vim-tmux-clipboard'
" Plug 'rrethy/vim-hexokinase', { 'do': 'make hexokinase' }
Plug 'ruanyl/vim-sort-imports'
Plug 'ryanoasis/vim-devicons'
" Plug 'sheerun/vim-polyglot'
Plug 'simeji/winresizer'
Plug 'slim-template/vim-slim'
Plug 'svermeulen/vim-cutlass'
Plug 'svermeulen/vim-subversive'
Plug 'svermeulen/vim-yoink'
Plug 'tmux-plugins/vim-tmux-focus-events'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-projectionist'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-rhubarb'
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-sleuth'
Plug 'tpope/vim-speeddating'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-unimpaired'
Plug 'valloric/listtoggle'
Plug 'vimwiki/vimwiki'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'wincent/terminus'
Plug 'yggdroot/indentline'

call plug#end()

call PlugHookPostGruvboxCommunityGruvbox()
call PlugHookPostNeovimLspconfig()
