# Install dotfiles
- Last step in the [boostrap-host](https://gitlab.com/jericdeleon/bootstrap-host) process.
- Take note of special instructions if the machine is a workstation (laptop / desktop), or a remote machine.

## A. Setup GPG
If remote, execute on remote machine.

1. Create `.gnupg`: `mkdir -p $HOME/.gnupg`
2. Download `.gpg.conf`: `wget -qO $HOME/.gnupg/gpg.conf https://gitlab.com/jericdeleon/dotfiles/-/raw/master/private_dot_gnupg/private_gpg.conf`
3. Download / edit `.gpg-agent.conf`: `wget -qO $HOME/.gnupg/gpg-agent.conf https://gitlab.com/jericdeleon/dotfiles/-/raw/master/private_dot_gnupg/private_gpg-agent.conf.tmpl`
4. Get GPG public key: `gpg --keyserver hkps://keys.openpgp.org --receive-keys 0x306A2FF94D024D5E`
5. Restart `gpg-agent`: `gpgconf --kill gpg-agent`

### Workstation

1. Configure `gpg` to use `scdaemon`:
    1. Download `scdaemon.conf`: `wget -qO $HOME/.gnupg/scdaemon.conf https://gitlab.com/jericdeleon/dotfiles/-/raw/master/private_dot_gnupg/private_scdaemon.conf`
    2. Kill running `scdaemon` processes: `pkill -9 scdaemon`

### Remote
1. In remote, add to last line of `/etc/ssh/sshd_config`: `StreamLocalBindUnlink yes`
2. In remote, restart `sshd`: `sudo systemctl restart sshd`
3. In remote / local, double check GPG sockets:
    - remote: `gpgconf --list-dir agent-socket`
    - local: `gpgconf --list-dir agent-extra-socket`
4. In local, add host entry to `~/.ssh/config`:
    - RemoteForward format: `remote_socket local_socket`
    ```
    Host server_name
      HostName server_ip
      User server_user
      ForwardAgent yes
      RemoteForward /run/user/1000/gnupg/S.gpg-agent /run/user/1000/gnupg/S.gpg-agent.extra
    ```

## B. Setup SSH
### Workstation
1. Configure SSH to use `gpg-agent`:
    ```
    export GPG_TTY="$(tty)"
    export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
    gpgconf --launch gpg-agent
    ```

### Remote
Make sure that workstation has valid `ssh-add -L` output.

1. From local, send SSH IDs: `ssh-copy-id server_user@server_host`

## C. Setup Git
1. Setup `.gitconfig` and replace variables: `wget -qO ~/.gitconfig https://gitlab.com/jericdeleon/dotfiles/-/raw/master/dot_gitconfig.tmpl`

## D. Install dotfiles

1. If remote, SSH into machine: `ssh server_user@server_name`
2. Test GPG: `gpg -k`, `gpg -K`
3. Test SSH: `ssh-add -L`
4. Test Git: `git config --global -l`
5. Add machine-specific data in `~/.config/chezmoi/chezmoi.toml`:
    - mac:
      ```
      [data]
        email = "jdeleon@workemail.com"
        pinentry = "/usr/local/bin/pinentry-mac"
        fontsize = "16.25"

      encryption = "gpg"
      [gpg]
        recipient = "mail@jericdeleon.com"
      ```
    - linux:
      ```
      [data]
        email = "dev@jericdeleon.com"
        pinentry = "/usr/bin/pinentry"
        fontsize = "11"

      encryption = "gpg"
      [gpg]
        recipient = "mail@jericdeleon.com"
      ```
6. Install and apply dotfiles:
    ```
    chezmoi init git@gitlab.com:jericdeleon/dotfiles.git
    chezmoi apply
    ```
7. Test on new terminal
    - `.zshrc`: Fix antidote
    - gpg setup (should work across restarts)

# Common Issues

## GPG
1. `gpg: OpenPGP card not available: No SmartCard daemon`
    - Install `scdaemon`
2. `gpg: OpenPGP card not available: No such device`
    - Install `pcscd`
3. `sign_and_send_pubkey: signing failed: agent refused operation`
    - Run `gpg-connect-agent updatestartuptty /bye`
4. PKDECRYPT failed: No secret key
    - Last invalid setup's pinentry is still being used, restart terminal then gpg --card-status
