" https://github.com/tpope/vim-fugitive/issues/1725
if exists('b:fugitive_type') && b:fugitive_type !=# 'blob'
  setlocal foldmethod=syntax
endif
