fg0='#282828'

bg0='#f2e5bc' # soft
# bg0="#fbf1c7" # medium
# bg0="#f9f5d7" # hard

bg1='#ebdbb2'
bg4='#a89984'

ansi_orange='#d65d0e'

bright_red='#9d0006'
bright_green='#79740e'
bright_yellow='#b57614'
bright_blue='#076678'
bright_purple='#8f3f71'

# special keys (cannot be overridden):
#   c-m (enter)
FZF_DEBUG_PIPE=/tmp/fzfdebug
[ -p $FZF_DEBUG_PIPE ] || mkfifo $FZF_DEBUG_PIPE
export FZF_DEFAULT_OPTS=" \
  --ansi \
  --bind \"ctrl-e:execute(${EDITOR} {} </dev/tty >/dev/tty)\" \
  --bind \"ctrl-d:execute(echo {} > $FZF_DEBUG_PIPE &)\" \
  --bind=ctrl-r:toggle \
  --bind=ctrl-t:toggle-all \
  --bind=ctrl-f:select \
  --bind=ctrl-g:select-all \
  --bind=ctrl-v:deselect \
  --bind=ctrl-b:deselect-all \
  --bind=ctrl-p:toggle-preview \
  --bind=ctrl-w:toggle-preview-wrap \
  --bind=ctrl-s:toggle-sort \
  --bind=ctrl-k:up \
  --bind=ctrl-j:down \
  --bind=ctrl-l:half-page-up \
  --bind=ctrl-h:half-page-down \
  --bind=ctrl-i:preview-up \
  --bind=ctrl-u:preview-down \
  --bind=ctrl-o:preview-half-page-up \
  --bind=ctrl-y:preview-half-page-down \
  --border \
  --color=fg:$bg4,bg:$bg0,hl:$bright_yellow \
  --color=fg+:$fg0,bg+:$bg1,hl+:$ansi_orange \
  --color=info:$bright_green,prompt:$bright_red,pointer:$bright_purple \
  --color=marker:$bright_green,spinner:$bright_purple,header:$bright_blue \
  --height='100%' \
  --preview='bat --style=numbers --color=always {}' \
  --preview-window='right,60%' \
  --reverse "

export FORGIT_FZF_DEFAULT_OPTS=" \
  --bind=ctrl-r:select-all \
  --bind=ctrl-p:toggle-preview \
  --height='100%' \
  --preview-window='down,85%' "

dl="$"
sq="'"
bs="\\\\\\" # double backslash (fzf must be passing this to a printf/echo cycle)
# 1st sed: remove git markers
# 2nd sed: remove first path when file is renamed in git
opts_editor="--bind \"ctrl-e:execute($EDITOR $dl(echo {} | sed -re ${sq}s|${bs}[.*${bs}](.*)|${bs}1|${sq} | sed -re ${sq}s|^.*  ->  (.*)|${bs}1|${sq} ) </dev/tty >/dev/tty)\""
export FORGIT_ADD_FZF_OPTS=$opts_editor
export FORGIT_DIFF_FZF_OPTS=$opts_editor

opts_commit="--preview-window='down,50%'"
export FORGIT_CHECKOUT_BRANCH_FZF_OPTS=$opts_commit

export FORGIT_COPY_CMD="pbcopy"
opts_commit_copy="${opts_commit} \
  --bind=ctrl-y:preview-half-page-down \
  --bind=\"ctrl-x:execute-silent(echo {} |grep -Eo '[a-f0-9]+' | head -1 | tr -d '[:space:]' |${FORGIT_COPY_CMD})\" "
export FORGIT_LOG_FZF_OPTS=$opts_commit_copy
export FORGIT_REBASE_FZF_OPTS=$opts_commit_copy
export FORGIT_FIXUP_FZF_OPTS=$opts_commit_copy
export FORGIT_COMMIT_FZF_OPTS=$opts_commit_copy
