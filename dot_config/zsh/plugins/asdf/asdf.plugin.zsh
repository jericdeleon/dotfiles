if [[ "$OSTYPE" == "darwin"* ]]; then
	# workaround: https://github.com/asdf-vm/asdf/issues/428
	# turnoff
	# export ASDF_DIR=$(brew --prefix asdf)
fi

if [[ "$OSTYPE" == "linux"* ]]; then
	export ASDF_DIR="$HOME"/.asdf
fi
