function yob() {
  local termconf=~/.alacritty.yml
  local tmuxconf=~/.tmux.conf
  local muttconf=~/.config/mutt/muttrc
  if [[ ! -f "${termconf}" ]] || [[ ! -f "${tmuxconf}" ]]; then
    echo "conf files not found"
    return 1
  fi

  local theme=$(cat "${termconf}" | grep 'colors:' | awk '{print $2}')
  case "${theme}" in
    *light)
      sed -i "" 's/*light/*dark/g' "${termconf}"
      sed -i "" 's/light/dark/g' "${tmuxconf}"
      tmux source-file "${tmuxconf}"
      sed -i "" 's/light/dark/g' "${muttconf}"
      export BAT_THEME="gruvbox-dark"
      ;;
    *dark)
      sed -i "" 's/*dark/*light/g' "${termconf}"
      sed -i "" 's/dark/light/g' "${tmuxconf}"
      tmux source-file "${tmuxconf}"
      sed -i "" 's/dark/light/g' "${muttconf}"
      export BAT_THEME="gruvbox-light"
      ;;
    *)
      echo "Invalid ${termconf} file"
      return 1
      ;;
  esac
}
