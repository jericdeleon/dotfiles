# running in scripts vs running in terminal will have different effects
# run closest to pin entry as possible
alias gpgt='gpg-connect-agent updatestartuptty /bye >/dev/null'
alias gpgs='gpg-connect-agent "scd serialno" "learn --force" /bye'
alias gpgk='gpgconf --kill gpg-agent '

# Encrypt the given file or directory to a given recipient
function gpge() {
  if [ "$#" -ne 2 ]; then
    echo "Usage: $0 FILE/DIRECTORY RECIPIENT" >&2
    return 1
  fi

  tar -c `basename $1` | gpg --encrypt --recipient $2 -o `basename $1`.tar.gpg
}

# Decrypt the given tar.gpg file
function gpgd() {
  if [ "$#" -ne 1  ] || [[ "$1" != *.tar.gpg ]]; then
    echo "Usage: $0 FILE.tar.gpg" >&2
    return 1
  fi

  gpg --quiet --decrypt $1 | tar -x
}

# if smartcard is found, setup GPG / SSH sockets (no need to do on remote)
SMARTCARD_FOUND=0
gpg --card-status &> /dev/null || SMARTCARD_FOUND=$?
if [[ $SMARTCARD_FOUND -eq 0 ]]; then
  # Enable gpg-agent if it is not running-
  # --use-standard-socket will work from version 2 upwards

  AGENT_SOCK=$(gpgconf --list-dirs | grep agent-socket | cut -d : -f 2)

  if [[ ! -S $AGENT_SOCK ]]; then
    gpg-agent --daemon --use-standard-socket &>/dev/null
  fi
  export GPG_TTY=$TTY

  # If smartcard is present, set SSH to use gpg-agent if it's enabled
  GNUPGCONFIG="${GNUPGHOME:-"$HOME/.gnupg"}/gpg-agent.conf"
  if [[ -r $GNUPGCONFIG ]] && command grep -q enable-ssh-support "$GNUPGCONFIG"; then
    export SSH_AUTH_SOCK="$AGENT_SOCK.ssh"
    unset SSH_AGENT_PID
  fi
fi
