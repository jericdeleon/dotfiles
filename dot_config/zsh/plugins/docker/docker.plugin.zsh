alias dcp='docker container prune'
alias dnp='docker network prune'
alias dvp='docker volume prune'
alias dei='docker exec -it '
alias dri='docker run --rm -it ' # run image and attach shell; ex: docker run -ti archlinux/archlinux /bin/bash

alias dril='docker run --rm -it -v ~/.local/share/ledgerfile/ledger.dat:/ledger.dat registry.gitlab.com/jericdeleon/ledger -f /ledger.dat '
