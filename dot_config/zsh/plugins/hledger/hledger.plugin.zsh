export LEDGER_FILE=~/.local/share/ledger/current.journal

alias hl='hledger '
alias hlu='hledger-ui '
alias hlut='hledger-ui --theme=terminal '
alias hlw='hledger-web '
