# gruvbox light theme

evaluate-commands %sh{
  bright_black="rgb:928374" # gray, second row
  ansi_red="rgb:cc241d"
  ansi_green="rgb:98971a"
  ansi_yellow="rgb:d79921"
  ansi_blue="rgb:458588"
  ansi_purple="rgb:b16286"
  ansi_aqua="rgb:689d6a"
  bright_white="rgb:3c3836" # fg, second row

  ansi_orange="rgb:d65d0e"

  bg="rgb:f2e5bc" # soft
  # bg="rgb:fbf1c7" # medium
  # bg="rgb:f9f5d7" # hard
  bg_alpha="rgba:f2e5bcf2"

  bg1="rgb:ebdbb2"
  bg2="rgb:d5c4a1"
  bg3="rgb:bdae93"
  bg4="rgb:a89984"

  fg="rgb:282828"

  fg1="rgb:3c3836"
  fg2="rgb:504945"
  fg3="rgb:665c54"
  fg3_alpha="rgba:665c54f2"
  fg4="rgb:7c6f64"

  echo "
    # Code highlighting
    face global value         ${ansi_purple}
    face global type          ${ansi_yellow}
    face global variable      ${ansi_blue}
    face global module        ${ansi_green}
    face global function      ${fg}
    face global string        ${ansi_green}
    face global keyword       ${ansi_red}
    face global operator      ${fg}
    face global attribute     ${ansi_orange}
    face global comment       ${bright_black}+i
    face global documentation comment
    face global meta          ${ansi_aqua}
    face global builtin       ${fg}+b

    # Markdown highlighting
    face global title     ${ansi_green}+b
    face global header    ${ansi_orange}
    face global mono      ${fg4}
    face global block     ${ansi_aqua}
    face global link      ${ansi_blue}+u
    face global bullet    ${ansi_yellow}
    face global list      ${fg}

    # Faces
    face global Default            ${fg},${bg}
    face global PrimarySelection   ${bg},${ansi_yellow}+g
    face global SecondarySelection ${bg2},${ansi_yellow}+g
    face global PrimaryCursor      ${bg},${fg2}+fg
    face global SecondaryCursor    ${bg},${bg3}+fg
    face global PrimaryCursorEol   ${bg},${fg2}+fg
    face global SecondaryCursorEol ${bg},${bg3}+fg

    face global MenuForeground     ${bg},${ansi_yellow}
    face global MenuBackground     ${fg3},${bg2}
    face global MenuInfo           ${fg4}

    face global Information        ${fg4},${bg1}
    face global Error              ${bg},${ansi_red}

    face global StatusLine         ${bg},${bg3}
    face global StatusLineMode     ${bg},${ansi_yellow}+b
    face global StatusLineInfo     ${bg},${ansi_purple}
    face global StatusLineValue    ${bg},${ansi_red}
    face global StatusCursor       ${bg3},${bg}

    face global Prompt             ${bg},${ansi_yellow}
    face global BufferPadding      ${bg2},${bg}

    # Highlighters
    face global LineNumbers        ${bg4}
    face global LineNumberCursor   ${ansi_yellow},${bg1}
    face global LineNumbersWrapped ${bg1}

    face global MatchingChar       ${fg},${bg3}+b
    face global Whitespace         ${bg2}+f
    # face global WrapMarker       
  "
}
