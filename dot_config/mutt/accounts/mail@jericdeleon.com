unmailboxes *

set my_email=mail@jericdeleon.com

source ~/.config/mutt/servers/fastmail

source ~/.config/mutt/sources/profile
source ~/.config/mutt/sources/servers
source ~/.config/mutt/sources/mailboxes
source ~/.config/mutt/sources/gpg
source ~/.config/mutt/sources/bindings_account
