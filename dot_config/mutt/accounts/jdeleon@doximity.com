unmailboxes *

set my_email=jdeleon@doximity.com

source ~/.config/mutt/servers/gmail

source ~/.config/mutt/sources/profile
source ~/.config/mutt/sources/servers
source ~/.config/mutt/sources/mailboxes
source ~/.config/mutt/sources/gpg
source ~/.config/mutt/sources/bindings_account
