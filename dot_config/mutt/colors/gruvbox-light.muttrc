# gruvbox light (contrast light):

# bg0    = 229 (made default)
# bg1    = 223
# bg2    = 250
# bg3    = 248
# bg4    = 246
#
# gray   = 245
#
# fg0    = 235
# fg1    = 237
# fg2    = 239
# fg3    = 241
# fg4    = 243
#
# red    = 88
# green  = 100
# yellow = 136
# blue   = 24
# purple = 96
# aqua   = 65
# orange = 130

# See http://www.mutt.org/doc/manual/#color

color attachment  color24 default
color bold        color235 default
color error       color88 default
color hdrdefault  color243 default
color indicator   color237 color223
color markers     color246 default
color normal      color237 default
color quoted      color239 default
color quoted1     color65 default
color quoted2     color239 default
color quoted3     color65 default
color quoted4     color239 default
color quoted5     color65 default
color search      default color130
color signature   color65 default
color status      default color250 # background used to be color239
color tilde       color246 default
color tree        color100 default
color underline   color237 color250

color sidebar_divider    color239 default
color sidebar_new        color100 default
color sidebar_unread     color65 default

color index color100 default ~N
color index color65 default ~O
color index color24 default ~P
color index color136 default ~F
color index color96 default ~Q
color index color88 default ~=
color index default color237 ~T
color index color136 color88 ~D # foreground used to be color229

color header color136 default "^(To:|From:)"
color header color100 default "^Subject:"
color header color65 default "^X-Spam-Status:"
color header color65 default "^Received:"

# BSD's regex has RE_DUP_MAX set to 255.
color body color100 default "[a-z]{3,255}://[-a-zA-Z0-9@:%._\\+~#=/?&,]+"
color body color100 default "[a-zA-Z]([-a-zA-Z0-9_]+\\.){2,255}[-a-zA-Z0-9_]{2,255}"
color body color130 default "[-a-z_0-9.%$]+@[-a-z_0-9.]+\\.[-a-z][-a-z]+"
color body color130 default "mailto:[-a-z_0-9.]+@[-a-z_0-9.]+"
color body default color136 "[;:]-*[)>(<lt;|]"
color body color235 default "\\*[- A-Za-z]+\\*"

color body color136 default "^-.*PGP.*-*"
color body color100 default "^gpg: Good signature from"
color body color88 default "^gpg: Can't.*$"
color body color136 default "^gpg: WARNING:.*$"
color body color88 default "^gpg: BAD signature from"
color body color88 default "^gpg: Note: This key has expired!"
color body color136 default "^gpg: There is no indication that the signature belongs to the owner."
color body color136 default "^gpg: can't handle these multiple signatures"
color body color136 default "^gpg: signature verification suppressed"
color body color136 default "^gpg: invalid node with packet of type"

color body color100 default "^Good signature from:"
color body color88 default "^.?BAD.? signature from:"
color body color100 default "^Verification successful"
color body color88 default "^Verification [^s][^[:space:]]*$"

color compose header            color237 default
color compose security_encrypt  color96 default
color compose security_sign     color24 default
color compose security_both     color100 default
color compose security_none     color130 default
