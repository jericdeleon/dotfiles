version = '0.21.1'

-- plugin manager
local home = os.getenv("HOME")
local xpm_path = home .. "/.local/share/xplr/dtomvan/xpm.xplr"
local xpm_url = "https://github.com/dtomvan/xpm.xplr"
package.path = package.path
  .. ";"
  .. xpm_path
  .. "/?.lua;"
  .. xpm_path
  .. "/?/init.lua"
os.execute(
  string.format(
    "[ -e '%s' ] || git clone '%s' '%s'",
    xpm_path,
    xpm_url,
    xpm_path
  )
)

-- plugins
require("xpm").setup({
  plugins = {
    { name = 'dtomvan/extra-icons.xplr' },
    { name = 'dtomvan/xpm.xplr' },
    { name = 'prncss-xyz/icons.xplr' },
    {
      name = 'sayanarijit/fzf.xplr',
      setup = function()
        require('fzf').setup {
          recursive = true,  -- If true, search all files under $PWD
          enter_dir = true,  -- Enter if the result is directory
        }
      end,
    },
    { name = 'sayanarijit/zentable.xplr' },
  },
  auto_install = true,
  auto_cleanup = true,
})

-- navigation
xplr.config.modes.builtin.default.key_bindings.on_key["ctrl-j"] = {
  help = "down 5 lines",
  messages = {
    { FocusNextByRelativeIndex = 5 }
  }
}
xplr.config.modes.builtin.default.key_bindings.on_key["ctrl-k"] = {
  help = "up 5 lines",
  messages = {
    { FocusPreviousByRelativeIndex = 5 }
  }
}
xplr.config.modes.builtin.default.key_bindings.on_key["e"] = {
  help = "open in editor",
  messages = {
    {
      BashExec0 = [===[
        ${EDITOR:-vi} "${XPLR_FOCUS_PATH:?}"
      ]===],
    },
    "PopMode",
  },
}

-- style
xplr.config.general.focus_ui.style.add_modifiers = { "Reversed" }
xplr.config.general.selection_ui.style.add_modifiers = { "Reversed", "Dim" }
xplr.config.general.focus_selection_ui.style.add_modifiers = { "Reversed" }

-- preview text in another pane
local function stat(node)
  return node.mime_essence
end
local function read(path, height)
  local p = io.open(path)

  if p == nil then
    return nil
  end

  local i = 0
  local res = ""
  for line in p:lines() do
    if line:match("[^ -~\n\t]") then
      p:close()
      return
    end

    res = res .. line .. "\n"
    if i == height then
      break
    end
    i = i + 1
  end
  p:close()

  return res
end
xplr.fn.custom.preview_pane = {}
xplr.fn.custom.preview_pane.render = function(ctx)
  local n = ctx.app.focused_node

  if n and n.canonical then
    n = n.canonical
  end

  if n then
    if n.is_file then
      return read(n.absolute_path, ctx.layout_size.height)
    else
      return stat(n)
    end
  else
    return ""
  end
end

-- layouts
xplr.config.general.initial_layout = "table_with_preview_hs_zen"
xplr.config.layouts.custom.table_with_preview_hs = {
  Vertical = {
    config = {
      constraints = {
        { Percentage = 90 },
        { Percentage = 10 },
      },
    },
    splits = {
      {
        Horizontal = {
          config = {
            constraints = {
              { Percentage = 20 },
              { Percentage = 30 },
              { Percentage = 50 },
            },
          },
          splits = {
            {
              Vertical = {
                config = {
                  constraints = {
                    { Percentage = 10 },
                    { Percentage = 20 },
                    { Percentage = 70 },
                  },
                },
                splits = {
                  "SortAndFilter",
                  "Selection",
                  "HelpMenu",
                },
              },
            },
            "Table",
            {
              CustomContent = {
                title = "preview",
                body = { DynamicParagraph = { render = "custom.preview_pane.render" } },
              },
            },
          },
        },
      },
      "InputAndLogs",
    }
  },
}
xplr.config.layouts.custom.table_with_preview_hs_zen = {
  Vertical = {
    config = {
      constraints = {
        { Percentage = 90 },
        { Percentage = 10 },
      },
    },
    splits = {
      {
        Horizontal = {
          config = {
            constraints = {
              { Percentage = 30 },
              { Percentage = 70 },
            },
          },
          splits = {
            "Table",
            {
              CustomContent = {
                title = "preview",
                body = { DynamicParagraph = { render = "custom.preview_pane.render" } },
              },
            },
          },
        },
      },
      "InputAndLogs",
    }
  },
}
xplr.config.layouts.custom.table_with_preview_vs = {
  Vertical = {
    config = {
      constraints = {
        { Percentage = 90 },
        { Percentage = 10 },
      },
    },
    splits = {
      {
        Horizontal = {
          config = {
            constraints = {
              { Percentage = 20 },
              { Percentage = 80 },
            },
          },
          splits = {
            {
              Vertical = {
                config = {
                  constraints = {
                    { Percentage = 10 },
                    { Percentage = 20 },
                    { Percentage = 70 },
                  },
                },
                splits = {
                  "SortAndFilter",
                  "Selection",
                  "HelpMenu",
                },
              },
            },
            {
              Vertical = {
                config = {
                  constraints = {
                    { Percentage = 30 },
                    { Percentage = 70 },
                  },
                },
                splits = {
                  "Table",
                  {
                    CustomContent = {
                      title = "preview",
                      body = { DynamicParagraph = { render = "custom.preview_pane.render" } },
                    },
                  },
                },
              }
            }
          },
        },
      },
      "InputAndLogs",
    }
  },
}
xplr.config.layouts.custom.table_with_preview_vs_zen = {
  Vertical = {
    config = {
      constraints = {
        { Percentage = 30 },
        { Percentage = 60 },
        { Percentage = 10 },
      },
    },
    splits = {
      "Table",
      {
        CustomContent = {
          title = "preview",
          body = { DynamicParagraph = { render = "custom.preview_pane.render" } },
        },
      },
      "InputAndLogs",
    }
  },
}

-- switch layout
xplr.config.modes.builtin.switch_layout = {
  name = "switch layout",
  layout = "HelpMenu",
  key_bindings = {
    on_key = {
      ["w"] = {
        help = "table_with_preview_hs_zen",
        messages = {
          { SwitchLayoutCustom = "table_with_preview_hs_zen" },
          "PopMode",
        },
      },
      ["s"] = {
        help = "table_with_preview_hs",
        messages = {
          { SwitchLayoutCustom = "table_with_preview_hs" },
          "PopMode",
        },
      },
      ["e"] = {
        help = "table_with_preview_vs_zen",
        messages = {
          { SwitchLayoutCustom = "table_with_preview_vs_zen" },
          "PopMode",
        },
      },
      ["d"] = {
        help = "table_with_preview_vs",
        messages = {
          { SwitchLayoutCustom = "table_with_preview_vs" },
          "PopMode",
        },
      },
    },
  },
}
